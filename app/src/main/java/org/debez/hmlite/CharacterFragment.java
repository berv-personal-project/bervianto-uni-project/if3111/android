package org.debez.hmlite;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.debez.hmlite.services.RestConnectService;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;

/**
 * Created by Silva on 2/23/2017.
 * Class for connecting to a rest
 */

public class CharacterFragment extends Fragment {

    private static final String TAG = CharacterFragment.class.getSimpleName();
    private static String url1 = "http://harvestmoonlite.herokuapp.com/getCharacterStatus/";
    public static int char_id;
    public static int inventory_id;

    private ServiceCaller caller;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_character, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FirebaseAuth auth = FirebaseAuth.getInstance();
        if (auth != null) {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user != null) {
                String UID = user.getUid();
                url1 += UID;
            }
        }
        caller = new ServiceCaller();
        caller.execute(url1);
    }

    public void update() {
        caller.execute(url1);
    }

    private class ServiceCaller extends AsyncTask<String, Void, Void> {
        private RestConnectService con;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... url) {
            try {
                con = new RestConnectService(new URL(url[0]), "GET");
                con.startReceive();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPreExecute();
            initializeUI(con.getResult());
        }
    }

    public void initializeUI(String result) {
        JSONObject var = null;
        try {
            var = new JSONObject(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Vector<String> data = new Vector<String>();
        ProgressBar bar = getView().findViewById(R.id.Experience_Bar);
        if (var != null) {
            try {
                char_id = var.getInt("character_id");
                data.add(0, var.getString("character_name"));
                data.add(1, Integer.toString(var.getInt("character_stamina")));
                data.add(2, Integer.toString(var.getInt("character_money")));
                data.add(3, "Level " + Integer.toString(var.getInt("character_level")));
                data.add(4, Integer.toString(var.getInt("character_exp")) + "/" + Integer.toString(var.getInt("character_next_exp")));
                bar.setMax(var.getInt("character_next_exp"));
                bar.setProgress(var.getInt("character_exp"));
                inventory_id = var.getInt("inventory_id");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (!data.isEmpty()) {
            TextView text = getView().findViewById(R.id.Name_value);
            text.setText(data.get(0));
            text = getView().findViewById(R.id.Stamina_value);
            text.setText(data.get(1));
            text = getView().findViewById(R.id.Gold_value);
            text.setText(data.get(2));
            text = getView().findViewById(R.id.Level_text);
            text.setText(data.get(3));
            text = getView().findViewById(R.id.exp);
            text.setText(data.get(4));
            if (HomeActivity.userPrefShared != null) {
                SharedPreferences.Editor editor = HomeActivity.userPrefShared.edit();
                editor.putString(HomeActivity.user_name_key, data.get(0));
                editor.putString(HomeActivity.user_stamina, data.get(1));
                editor.putString(HomeActivity.user_gold, data.get(2));
                editor.putString(HomeActivity.user_level, data.get(3));
                editor.putString(HomeActivity.user_exp, data.get(4));
                editor.apply();
            }
        } else {
            if (HomeActivity.userPrefShared != null) {
                String input = HomeActivity.userPrefShared.getString(HomeActivity.user_name_key, null);
                TextView text;
                if (input != null) {
                    text = getView().findViewById(R.id.Name_value);
                    text.setText(input);
                }
                input = HomeActivity.userPrefShared.getString(HomeActivity.user_stamina, null);
                if (input != null) {
                    text = getView().findViewById(R.id.Stamina_value);
                    text.setText(input);
                }
                input = HomeActivity.userPrefShared.getString(HomeActivity.user_gold, null);
                if (input != null) {
                    text = getView().findViewById(R.id.Gold_value);
                    text.setText(input);
                }
                input = HomeActivity.userPrefShared.getString(HomeActivity.user_level, null);
                if (input != null) {
                    text = getView().findViewById(R.id.Level_text);
                    text.setText(input);
                }
                input = HomeActivity.userPrefShared.getString(HomeActivity.user_exp, null);
                if (input != null) {
                    text = getView().findViewById(R.id.exp);
                    text.setText(input);
                }
            }
        }
    }
}
