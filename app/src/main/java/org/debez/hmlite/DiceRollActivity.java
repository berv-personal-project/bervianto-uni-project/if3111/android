package org.debez.hmlite;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.debez.hmlite.services.RestConnectService;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;
import java.util.concurrent.ExecutionException;

/**
 * Created by Silva on 2/23/2017.
 * Class for connecting to a rest
 */

public class DiceRollActivity extends AppCompatActivity implements SensorEventListener {
    private SensorManager mSensorManager;
    private TextView accValue;
    private boolean test;
    private double lastUpdate;
    private Sensor mAccelerometer;
    private getData dataItem;
    private postResult itemDetails;

    private final String TAG = DiceRollActivity.class.getSimpleName();
    private final String url1 = "https://harvestmoonlite.herokuapp.com/getGachaItemID/";
    private String url2 = "https://harvestmoonlite.herokuapp.com/addItem/";
    private RestConnectService con;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        try {
            dataItem = new getData();
            dataItem.execute(url1).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        setContentView(R.layout.activity_dice_roll);
        accValue = findViewById(R.id.resultButton);

        // sensor
        test = true;
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        if (mSensorManager != null)
            mSensorManager.registerListener(DiceRollActivity.this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        lastUpdate = System.currentTimeMillis();
    }

    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do something here if sensor accuracy changes.
    }

    @Override
    public final void onSensorChanged(SensorEvent event) {
        float[] values = event.values;
        // Movement
        float x = values[0];
        float y = values[1];
        float z = values[2];

        float accelationSquareRoot = (x * x + y * y + z * z)
                / (SensorManager.GRAVITY_EARTH * SensorManager.GRAVITY_EARTH);
        long actualTime = event.timestamp;

        if (test && (actualTime - lastUpdate > 200) && accelationSquareRoot > 5) {
            //accValue.setText(x + ", " + y + ", " + z);
            lastUpdate = System.currentTimeMillis();
            JSONObject random_range = null;
            try {
                random_range = new JSONObject(con.getResult());
                if (random_range.length() > 0) {
                    //Toast.makeText(DiceRollActivity.this, getString(R.string.success_get_data), Toast.LENGTH_LONG).show();
                } else {
                    //Toast.makeText(DiceRollActivity.this, getString(R.string.no_connection), Toast.LENGTH_LONG).show();
                }
                String Param = rollDice(random_range);
                if (Param.compareToIgnoreCase("error") == 0) {
                    accValue.setText(getString(R.string.no_item));
                    Toast.makeText(DiceRollActivity.this, getString(R.string.no_connection), Toast.LENGTH_LONG).show();
                } else {
                    Param = getString(R.string.item_get_message) + " " + Param;
                    accValue.setText(Param);
                    Toast.makeText(DiceRollActivity.this, getString(R.string.data_sent_success), Toast.LENGTH_LONG).show();
                }
                test = false;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String rollDice(JSONObject random_range) {
        Random random = new Random(System.currentTimeMillis());
        JSONArray item;
        int item_id;
        try {
            String result;
            item = random_range.getJSONArray("item");
            item_id = item.getInt(random.nextInt(100) % item.length());
            url2 += CharacterFragment.inventory_id + "/" + item_id;
            new postResult().execute(url2).get();
            JSONObject res;
            res = new JSONObject(con.getResult());
            result = res.getString("item_name");
            return result;
        } catch (Exception e) {
            return "Error";
        }
    }

    public void out(View view) {
        //do nothing
    }

    private boolean checkSensor(int Type) {
        return mSensorManager.getDefaultSensor(Type) != null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        // unregister listener
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    private class getData extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Toast.makeText(DiceRollActivity.this, getString(R.string.loading_data_message), Toast.LENGTH_LONG).show();
        }

        @Override
        protected Void doInBackground(String... url) {
            try {
                con = new RestConnectService(new URL(url[0]), "GET");
                con.startReceive();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPreExecute();
        }
    }

    private class postResult extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                con = new RestConnectService(new URL(params[0]), "GET");
                con.startReceive();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }
}
