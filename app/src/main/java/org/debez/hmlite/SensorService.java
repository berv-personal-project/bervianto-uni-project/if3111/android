package org.debez.hmlite;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by adesu on 25/02/2017.
 */

public class SensorService extends Service implements SensorEventListener {
    /**
     * a tag for logging
     */
    private static final String TAG = SensorService.class.getSimpleName();

    /**
     * again we need the sensor manager and sensor reference
     */
    private SensorManager mSensorManager = null;
    private Sensor proxSensor;
    private final IBinder binder = new LocalBinder();
    // Registered callbacks
    private ServiceCallbacks serviceCallbacks;
    /**
     * an optional flag for logging
     */
    /**
     * also keep track of the previous value
     */
    public static final String KEY_SENSOR_TYPE = "sensor_type";

    public static final String KEY_LOGGING = "logging";

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        // get sensor manager on starting the service
        // Binder given to clients
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        proxSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        // have a default sensor configured
        int sensorType = Sensor.TYPE_PROXIMITY;

        Bundle args = intent.getExtras();

        // get some properties from the intent
        if (args != null) {
            // set sensortype from bundle
            if (args.containsKey(KEY_SENSOR_TYPE))
                sensorType = args.getInt(KEY_SENSOR_TYPE);
        }
        // TODO we could have the sensor reading delay configurable also though that won't do much
        // in this use case since we work with the alarm manager
        mSensorManager.registerListener(this, proxSensor,
                SensorManager.SENSOR_DELAY_NORMAL);

        return START_STICKY;
    }

    public class LocalBinder extends Binder {
        SensorService getService() {
            // Return this instance of MyService so clients can call public methods
            return SensorService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public void setCallbacks(ServiceCallbacks callbacks) {
        serviceCallbacks = callbacks;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor,int accuracy){

    }

    @Override
    public void onDestroy(){
        mSensorManager.unregisterListener(this);
        stopSelf();
    }

    @Override
    public void onSensorChanged(SensorEvent event){
        //proxText.setText(String.valueOf(event.values[0]));
        if(event.values[0] == 0){
            if (serviceCallbacks != null) {
                serviceCallbacks.doSomething();
            }
            Log.v("Debug", "event value == 0 ..");
        }
    }
}