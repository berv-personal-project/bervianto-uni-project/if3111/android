package org.debez.hmlite;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import org.debez.hmlite.services.RestConnectService;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PlantDetails extends AppCompatActivity {

    private RestConnectService con;
    private String url = "https://harvestmoonlite.herokuapp.com/getPlantInfo/";
    private CountDownTimer deathTime;
    private CountDownTimer harvestTime;
    private CountDownTimer growthTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plant_details);

        Intent comingIntent = getIntent();
        int plotID = comingIntent.getIntExtra(PlantActivity.EXTRA_MESSAGE, 0);
        if (plotID == 0) {
            // TODO, back to activity before
        } else {
            url += plotID;
            new getData().execute(url);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (deathTime != null) {
            deathTime.cancel();
        }
        if (harvestTime != null) {
            harvestTime.cancel();
        }
        if (growthTime != null) {
            growthTime.cancel();
        }
    }

    private class getData extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Toast.makeText(PlantDetails.this, getString(R.string.loading_data_message), Toast.LENGTH_LONG).show();
        }

        @Override
        protected Void doInBackground(String... url) {
            try {
                con = new RestConnectService(new URL(url[0]), "GET");
                con.startReceive();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPreExecute();
            String receive = con.getResult();
            if (receive != null) {
                try {
                    JSONObject plant = new JSONObject(receive);
                    int plant_id = plant.getInt("plant_id");
                    int plant_phase = plant.getInt("plant_phase");
                    String creature_name = plant.getString("creature_name");
                    String death_time = "";
                    if (!plant.isNull("plant_death_time")) {
                        death_time = plant.getString("plant_death_time");
                    }
                    String harvest_time = "";
                    if (!plant.isNull("plant_harvest_time")) {
                        harvest_time = plant.getString("plant_harvest_time");
                    }
                    String time_growth = "";
                    if (!plant.isNull("plant_till_growth")) {
                        time_growth = plant.getString("plant_till_growth");
                    }

                    Plant myPlant = new Plant(plant_id, plant_phase, creature_name, death_time, harvest_time, time_growth);

                    // TODO Proccess the time

                    // TODO Proccess the plant status

                    TextView plantName = (TextView) findViewById(R.id.plant_name);
                    plantName.setText(myPlant.getCreatureName());

                    SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    death_time = myPlant.getPlantDeathTime();
                    try {
                        Date date = parser.parse(death_time);
                        long now = System.currentTimeMillis();
                        long death = date.getTime();
                        long range = death - now;
                        if (range >  0) {
                            deathTime = new CountDownTimer(range, 1000) {
                                TextView timeHungger = (TextView) findViewById(R.id.time_hungger);

                                @Override
                                public void onTick(long millisUntilFinished) {
                                    long totalsecond = millisUntilFinished / 1000;
                                    long hours = totalsecond / 3600;
                                    long sisa = totalsecond - (hours * 3600);
                                    long minutes = sisa / 60;
                                    sisa = sisa - (minutes * 60);
                                    long seconds = sisa;
                                    timeHungger.setText(hours + " hours " + minutes + " minutes " + seconds + " seconds");
                                }

                                @Override
                                public void onFinish() {
                                    // TODO Notification
                                    timeHungger.setText(getString(R.string.death_text));
                                }
                            }.start();

                            /* Harvest Time */
                            harvest_time = myPlant.getPlantHarvestTime();
                            if (!harvest_time.equalsIgnoreCase("")) {
                                try {
                                    date = parser.parse(harvest_time);
                                    now = System.currentTimeMillis();
                                    long harvest = date.getTime();
                                    if (now - harvest > 0) {
                                        TextView timeHarvest = (TextView) findViewById(R.id.time_harvest);
                                        timeHarvest.setText(getString(R.string.ready_harvest));
                                    } else {
                                        deathTime = new CountDownTimer(harvest - now, 1000) {
                                            TextView timeHarvest = (TextView) findViewById(R.id.time_harvest);

                                            @Override
                                            public void onTick(long millisUntilFinished) {
                                                long totalsecond = millisUntilFinished / 1000;
                                                long hours = totalsecond / 3600;
                                                long sisa = totalsecond - (hours * 3600);
                                                long minutes = sisa / 60;
                                                sisa = sisa - (minutes * 60);
                                                long seconds = sisa;
                                                timeHarvest.setText(hours + " hours " + minutes + " minutes " + seconds + " seconds");
                                            }

                                            @Override
                                            public void onFinish() {
                                                // TODO Notification
                                                timeHarvest.setText(getString(R.string.ready_harvest));
                                            }
                                        }.start();
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                TextView timeHarvest = (TextView) findViewById(R.id.time_harvest);
                                timeHarvest.setText(getString(R.string.not_ready_to_harvest));
                            }

                            /* Growth Time */
                            time_growth = myPlant.getPlantHarvestTime();
                            if (time_growth.equalsIgnoreCase("")) {
                                TextView timeGrowth = (TextView) findViewById(R.id.time_growth);
                                timeGrowth.setText(getString(R.string.not_growing));
                            } else {
                                try {
                                    date = parser.parse(time_growth);
                                    now = System.currentTimeMillis();
                                    long growth = date.getTime();
                                    if (now - growth < 0) {
                                        deathTime = new CountDownTimer(growth - now, 1000) {
                                            TextView timeGrowth = (TextView) findViewById(R.id.time_growth);

                                            @Override
                                            public void onTick(long millisUntilFinished) {
                                                long totalsecond = millisUntilFinished / 1000;
                                                long hours = totalsecond / 3600;
                                                long sisa = totalsecond - (hours * 3600);
                                                long minutes = sisa / 60;
                                                sisa = sisa - (minutes * 60);
                                                long seconds = sisa;
                                                timeGrowth.setText(hours + " hours " + minutes + " minutes " + seconds + " seconds");
                                            }

                                            @Override
                                            public void onFinish() {
                                                // TODO Notification
                                                timeGrowth.setText(getString(R.string.growth));
                                            }
                                        }.start();
                                    } else {
                                        TextView timeGrowth = (TextView) findViewById(R.id.time_growth);
                                        timeGrowth.setText(getString(R.string.growth));
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }

                        } else {
                            TextView timeHungger = (TextView) findViewById(R.id.time_hungger);
                            timeHungger.setText(getString(R.string.death_text));
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    TextView plantStatus = (TextView) findViewById(R.id.status_plant);
                    plantStatus.setText(String.valueOf(myPlant.getPlantPhase()));

                    ImageView image = (ImageView) findViewById(R.id.plant_image);
                    /* Configure Picture */
                    if (myPlant.getPlantPhase() < 2) {
                        image.setImageResource(R.drawable.plant_muda);
                    } else if (myPlant.getPlantId() == 2) {
                        image.setImageResource(R.drawable.plant_remaja);
                    } else {
                        image.setImageResource(R.drawable.plant_tua);
                    }
                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }
        }
    }
}