package org.debez.hmlite.auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

import org.debez.hmlite.HomeActivity;
import org.debez.hmlite.R;

/**
 * Created by Bervianto Leo P on 25/02/2017.
 */

public class SignupActivity extends AppCompatActivity {
    // UI Component
    private EditText inputName, inputEmail, inputPassword;
    private Button btnSignIn, btnSignUp, btnResetPassword;
    private ProgressBar progressBar;

    private FirebaseAuth auth;
    private static final String TAG = "SignUpActivity";
    private static String url1 = "http://harvestmoonlite.herokuapp.com/createAccount/";
    protected RequestQueue queue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        //Get Firebase auth instance
        auth = FirebaseAuth.getInstance();

        // setup UI component
        btnSignIn = findViewById(R.id.sign_in_button);
        btnSignUp = findViewById(R.id.sign_up_button);
        inputName = findViewById(R.id.name);
        inputEmail = findViewById(R.id.email);
        inputPassword = findViewById(R.id.password);
        progressBar = findViewById(R.id.progressBar);
        btnResetPassword = findViewById(R.id.btn_reset_password);

        btnResetPassword.setOnClickListener(v -> startActivity(new Intent(SignupActivity.this, ResetPasswordActivity.class)));

        btnSignIn.setOnClickListener(v -> finish());
        btnSignUp.setOnClickListener(v -> {

            final String name = inputName.getText().toString().trim();
            String email = inputEmail.getText().toString().trim();
            String password = inputPassword.getText().toString().trim();

            if (TextUtils.isEmpty(name)) {
                Toast.makeText(getApplicationContext(), getString(R.string.empty_name_input), Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(email)) {
                Toast.makeText(getApplicationContext(), getString(R.string.empty_email_input), Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(password)) {
                Toast.makeText(getApplicationContext(), getString(R.string.empty_password_input), Toast.LENGTH_SHORT).show();
                return;
            }
            if (password.length() < 6) {
                Toast.makeText(getApplicationContext(), getString(R.string.minimum_password), Toast.LENGTH_SHORT).show();
                return;
            }

            progressBar.setVisibility(View.VISIBLE);
            //create user
            auth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(SignupActivity.this, task -> {
                        progressBar.setVisibility(View.GONE);
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(SignupActivity.this, getString(R.string.register_failed),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            FirebaseUser newUser = FirebaseAuth.getInstance().getCurrentUser();
                            if (newUser != null) {
                                UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                        .setDisplayName(name)
                                        .build();

                                newUser.updateProfile(profileUpdates)
                                        .addOnCompleteListener(task1 -> {
                                            if (task1.isSuccessful()) {
                                                Log.d(TAG, "User profile updated.");
                                                FirebaseUser newUser1 = FirebaseAuth.getInstance().getCurrentUser();
                                                if (newUser1 != null) {
                                                    String uid = newUser1.getUid();
                                                    String the_name = newUser1.getDisplayName();
                                                    String send_url = url1 + uid + "/" + the_name;
                                                    // Request a string response from the provided URL.
                                                    StringRequest stringRequest = new StringRequest(Request.Method.GET, send_url,
                                                            response -> {
                                                                // Display the first 500 characters of the response string.
                                                                // mTextView.setText("Response is: "+ response.substring(0,500));
                                                                Log.i(TAG, "Responses is:" + response.substring(0, 500));
                                                            }, error -> {
                                                                //mTextView.setText("That didn't work!");
                                                                Log.i(TAG, "That didn't work");
                                                            });
                                                    // Add the request to the RequestQueue.
                                                    queue.add(stringRequest);
                                                    startActivity(new Intent(SignupActivity.this, HomeActivity.class));
                                                    finish();
                                                }
                                            }
                                        });


                            }
                        }
                    });

        });
        queue = Volley.newRequestQueue(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }

}