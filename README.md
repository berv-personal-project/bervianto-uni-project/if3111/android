# Android Application

## Tujuan dan Fungsi Subsistem (Android)
Tujuan dari diadakan subsistem adalah untuk mendukung sistem utama (game harvest moon lite) yang ada di unity/desktop.

Fungsinya akan memberikan item (dari gatcha) ke pemain, menampilkan status pemain, menampilkan status tamanan pemain.

Selain itu mendapatkan lokasi dari pengguna yang akan dikirimkan ke dalam permainan.


## Daftar Fitur

a. Register, Login, Reset Password

![Register](screenshot/register.jpg)
![Login](screenshot/login.jpg)
![Reset](screenshot/reset.jpg)

Halaman ini berfungsi untuk mendaftarkan akun, masuk ke dalam akun dan mereset password pada akun yang sudah ada.

Dalam halaman ini tersedia sharedPreferences yang akan menyimpan email yang pernah dimasukan pemain dalam halaman ini.

b. HomeScreen

![HomeScreen](screenshot/home.jpg)

Home Screen ini sebagai halaman utama saat masuk dengan akun dan akan menampilkan status pengguna.

Dalam halaman ini mendeteksi lokasi pemain untuk update layar dashboard dan mendeteksi proximity yang bisa menutup aplikasi secara otomatis.

c. Gatcha

![Gatcha](screenshot/gatcha.jpg)

Memberikan item terbaik dan didapatkan berdasarkan keberuntungan.

Nilai keberuntungan akan berdasarkan guncangan yang dihasilkan dan keberuntungan pemain sendiri.

d. Plan Status

![Plan](screenshot/plan.jpg)

Menampilkan status dari tanaman yang ada dan yang ditanam.

## Daftar Sensor

* Sensor Proximity

Hanya berlaku di home screen, untuk menutup aplikasi secara otomatis dengan membalikan smartphone.

* Sensor Accleration

Sensor ini digunakan untuk mendapatkan sebuah nilai yang akan diproses menjadi angka random sehingga menghasilkan sebuah hadiah yang akan dimasukan dalam server dan diterima oleh pengguna.

## Daftar Service

* Service Proximity

Service ini akan memastikan untuk proximity tetap dimonitor dan selanjutnya dapat digunakan pada activity lainnya.
* Google Cloud Messaging Service

Akan mendapatkan notifikasi dari Admin Game.

* Google Location Service

Service ini akan mengirimkan data lokasi pengguna untuk disimpan dalam server. Kemudian game yang terintegrasi dengan server tersebut bahasa yang digunakan akan menyesuakan dengan lokasi negara di database server

# About

1. Bervianto Leo P - 13514047
2. Ade Surya Ramadhani - 13514049
3. M. Az-zahid Adhitya Silparensi - 13514095